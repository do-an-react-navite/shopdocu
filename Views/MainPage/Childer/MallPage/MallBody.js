//import liraries
import React, { useEffect } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  FlatList,
  TouchableOpacity,
  Modal,
} from "react-native";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import { AntDesign } from "@expo/vector-icons";
import { MaterialIcons } from "@expo/vector-icons";
//

import { useDispatch, useSelector } from "react-redux";

import { useState } from "react";
import { fetAllCourses } from "../../../../redux/actions/couseAction";

const MallBody = ({ handleClick }) => {
  {
    //

    const dispatch = useDispatch();
    const db = useSelector((store) => store.couses);

    const [Data, setData] = useState([]);
    console.log("data", db);
    //set 3

    useEffect(() => {
      dispatch(fetAllCourses());
      setData(db.couses);
    }, []);
    //
    //compoment view

    const ModalInfo = ({ modalVisible, setModalVisible, item }) => {
      return (
        <Modal
          animationType="slide"
          transparent={true}
          visible={modalVisible}
          onRequestClose={() => {
            setModalVisible(!modalVisible);
          }}
        >
          <View
            style={{
              flex: 1,
              backgroundColor: "#fff",

              alignItems: "center",
            }}
          >
            <TouchableOpacity
              onPress={() => {
                setModalVisible(false);
              }}
            >
              <MaterialIcons name="cancel" size={24} color="black" />
            </TouchableOpacity>
            <Text
              style={{
                fontWeight: "bold",
                justifyContent: "center",
                alignContent: "center",
                fontSize: 20,
                color: "red",
                marginBottom: 130,
                marginTop: 100,
              }}
            >
              Thông Tin Sản Phẩm
            </Text>
            <View>
              <Text style={styles.textitem}>
                Tên Sản Phẩm: {item.productName}
              </Text>
              <Text style={styles.textitem}>
                Trạng Thái: {item.Description}{" "}
              </Text>
              <Text style={styles.textitem}>size: {item.Size} </Text>
            </View>
          </View>
        </Modal>
      );
    };
    const TextInputWithModal = ({ item }) => {
      const [modalVisible, setModalVisible] = React.useState(false);
      return (
        <View style={styles.container}>
          <TouchableOpacity
            onPress={() => {
              setModalVisible(true);
            }}
          >
            <FontAwesome name="info-circle" color="black" size={30} />
          </TouchableOpacity>
          <ModalInfo
            modalVisible={modalVisible}
            setModalVisible={setModalVisible}
            item={item}
          />
        </View>
      );
    };

    const navigate = (id) => {
      // console.log(id)
      navigation.navigate("Details", {
        id: id,
      });
    };

    return (
      <View style={styles.container}>
        <FlatList
          data={db.couses}
          renderItem={({ item, index }) => {
            return (
              <View style={styles.itemlist}>
                <View style={styles.itemimg}>
                  <Image
                    style={styles.logo}
                    source={{ uri: item.productImage }}
                  />
                </View>

                <View style={styles.textitem}>
                  <Text style={styles.text}>
                    Tên Sản Phẩm: {item.productName}
                  </Text>
                  <Text style={styles.text}>Giá Sản Phẩm: {item.Price}</Text>
                  <View style={styles.listbtn}>
                    <TouchableOpacity onPress={() => navigate(item.id)}>
                      <View style={styles.item} key={item.id}>
                        <TextInputWithModal item={item} />
                      </View>
                    </TouchableOpacity>
                  </View>
                  <TouchableOpacity style={styles.btn}>
                    <Text
                      onPress={() => handleClick(item)}
                      style={styles.textbtn}
                    >
                      Mua
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
            );
          }}
        />
      </View>
    );
  }
};

// define your styles
const styles = StyleSheet.create({
  container: {
    flex: 6,
    backgroundColor: "#009387",
  },
  itemlist: {
    padding: 20,
    flexDirection: "row",
    backgroundColor: "#fff",
    margin: 20,
    borderRadius: 20,
    overflow: "hidden",
  },
  logo: {
    width: 100,
    height: 100,
  },
  itemimg: {
    borderColor: "red",
    borderWidth: 2,
    borderRadius: 10,
  },
  textitem: {
    justifyContent: "center",
    marginLeft: 40,
    fontSize: 17,
    marginBottom: 20,
  },
  btn: {
    backgroundColor: "red",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 10,
    width: 70,
    height: 30,
    marginTop: 20,
    marginLeft: 10,
  },
  textbtn: {
    color: "#fff",
  },
  text: {
    fontWeight: "bold",
  },
  listbtn: {
    flexDirection: "row",
  },
  textBox: {
    fontFamily: "Quicksand-Medium",
    marginRight: 20,
  },
});

//make this component available to the app
export default MallBody;
