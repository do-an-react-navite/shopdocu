//import liraries
import React, { Component, useState } from "react";
import { View, Text, StyleSheet } from "react-native";
import MallBody from "./MallBody";

import MallHeader from "./MallHeader";
import { List } from "antd";
import { useEffect } from "react";

// create a component
const MallPage = ({ navigation }) => {
  const [cart, setCart] = useState("");
  const [show, setShow] = useState(true);
  const handleClick = (item) => {
    // if (cart.indexOf(item) !== 1) return;
    setCart([...cart, item]);
    console.log(cart);
  };

  useEffect(() => {
    console.log("cart Change", cart);
  }, [cart]);
  return (
    <View style={styles.container}>
      <MallHeader />
      <MallBody handleClick={handleClick} />
    </View>
  );
};

// define your styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#009387",
  },
});

//make this component available to the app
export default MallPage;
