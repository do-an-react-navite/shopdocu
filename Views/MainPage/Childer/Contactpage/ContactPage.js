//import liraries
import React, { Component } from "react";
import { View, Text, StyleSheet, TouchableOpacity, Alert } from "react-native";
import { getAuth, signOut } from "firebase/auth";
import { firebaseConfig } from "../../../../config/firebase";
import { initializeApp } from "firebase/app";
import ContactHeader from "./ContactHeader";

// create a component
const ContactPage = ({ navigation }) => {
  const auth = getAuth(app);
  const app = initializeApp(firebaseConfig);
  const handleSigOut = () => {
    signOut(auth)
      .then(() => {
        navigation.replace("Login");
      })
      .catch((err) => {
        console.log(err);
        Alert.alert(err.message);
      });
  };
  return (
    <View style={styles.container}>
      <ContactHeader />
      <View style={styles.containerbody}>
      
        <View style={styles.btn}> 
          <TouchableOpacity style={styles.LogOut} onPress={handleSigOut}>
            <Text style={styles.textSign}>Log Out</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

// define your styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#009387",
  },
  containerbody: {
    flex: 2,
    backgroundColor: "#009387",
  },
  texx: {
    color: "#fff",
    fontSize: 20,
    fontWeight: "bold",
    margin: 30,
  },
  btn: {
    alignItems: "center",
    marginTop: 50,
  },
  LogOut: {
    width: "80%",
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 10,
    backgroundColor: "#009387",

    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.3,
    shadowRadius: 4.65,

    elevation: 8,
  },
  textSign: {
    fontSize: 18,
    fontWeight: "bold",
  },
});

//make this component available to the app
export default ContactPage;
