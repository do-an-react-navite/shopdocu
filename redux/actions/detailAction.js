export const GET_DETAIL = "GET_DETAIL";

export const getAllcourses = (courses) => {
  return {
    type: GET_DETAIL,
    payload: courses,
  };
};
/// action thunk
export const fetAllDetail = () => {
  return (dispatch) => {
    const getData = async () => {
      const courses = await fetch("http://localhost:5555/api/sanpham/:id");
      const data = await courses.json();
      console.log(data);
      //step 2 call action redux
      dispatch(fetAllDetail(data));
    };
    getData();
  };
};
