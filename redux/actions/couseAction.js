export const GET_ALL = "GET_ALL";


export const getAllcourses = (courses) => {
  return {
    type: GET_ALL,
    payload: courses,
  };
};
/// action thunk
export const fetAllCourses = () => {
  return (dispatch) => {
    const getData = async () => {
      const courses = await fetch("http://localhost:5555/api/sanpham");
      const data = await courses.json();
      console.log(data);
      //step 2 call action redux
      dispatch(getAllcourses(data));
    };
    getData();
  };
};
