import { GET_ALL } from "../actions/couseAction";
import { GET_DETAIL } from "../actions/detailAction";

const initialState = {
  couses: [],
};
export const coursesReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_ALL: {
      return {
        ...state,
        couses: action.payload,
      };
    }
    case GET_DETAIL: {
      return {
        ...state,
        couses: action.payload,
      };
    }
    default:
      return { ...state };
  }
};
