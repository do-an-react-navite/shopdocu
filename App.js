import { StyleSheet, Text, View } from "react-native";

import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";



import { Provider } from "react-redux";
import store from "./redux/stores/store";
import MainHome from "./Home/MainHome";
import MallBody from "./Views/MainPage/Childer/MallPage/MallBody";
import MallPage from "./Views/MainPage/Childer/MallPage/MallPage";

const Stack = createNativeStackNavigator();

export default function App() {
  return (
    <Provider store={store}>
      {/* <NavigationContainer>
        <MainHome />
      </NavigationContainer> */}
      <MallPage/>
    </Provider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
});
